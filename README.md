This code is responsible for creating, sending and accepting game invitations.
There are several issues with it that we'd like you to identify.
Some are basic, others are more advanced.

In addition, we'd like you to add invitation redelivery for private (closed) invitations.
Admin needs to see a button next to each closed invitation. Clicking it will deliver
invitation e-mail again, just like when new invitation is created.