require "rails_helper"

RSpec.describe GameAccess::InvitationMailer, type: :mailer do

  describe ".invite_user" do
    let(:creator)    { mock_model(User, email: "luke@jedi.org", full_name: "Luke Skywalker") }
    let(:game)       { mock_model(Game, name: "Chrome Wars") }
    let(:invitation) { mock_model(GameAccess::Invitation, token: "123-TOKEN", game: game, creator: creator) }

    let(:mail)       { GameAccess::InvitationMailer.invite_user(invitation, "invited_person@example.com") }

    it "builds an e-mail that will be send to the invited person e-mail" do
      expect(mail.from).to    eq(["no-reply@alludolearning.com"])
      expect(mail.to).to      eq(["invited_person@example.com"])
      expect(mail.subject).to eq("Luke Skywalker invited you to Chrome Wars")
    end

    it "renders e-mail with link to accept invitation" do
      expect(mail.body).to include("<h2>Welcome!</h2>")
      expect(mail.body).to include("<strong><a href=\"mailto:luke@jedi.org\">Luke Skywalker</a></strong>\nhas invited you to <strong>Chrome Wars</strong>.")
      expect(mail.body).to include_ahoy_message_pixel
      expect(mail.body).to include_ahoy_tracked_link(
        url: "http://example.com/game_access/invitations/123-TOKEN/accept?invited_email=invited_person%40example.com",
        label: "Join Chrome Wars",
        utm_campaign: "invite_user",
        mailer_name: "game_access/invitation_mailer"
      )
    end
  end

end
