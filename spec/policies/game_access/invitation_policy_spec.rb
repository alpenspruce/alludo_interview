require 'rails_helper'

RSpec.describe GameAccess::InvitationPolicy do
  subject { described_class }

  let!(:game) { create(:game) }

  let(:admin)                { create(:game_access_grant, :admin,                game: game).user }
  let(:admin_inactive)       { create(:game_access_grant, :admin, :inactive,     game: game).user }
  let(:player)               { create(:game_access_grant, :player,               game: game).user }
  let(:roleless)             { create(:game_access_grant, :roleless,             game: game).user }
  let(:no_grant)             { create(:user) }

  let(:invitation) { GameAccess::Invitation.new(game: game) }

  describe "#permitted_attributes" do
    it "returns all editable attributes for new records" do
      policy = described_class.new(admin, GameAccess::Invitation.new(game: game))

      expect(policy.permitted_attributes).to eq([
        :invited_emails,
        :open,
        :active,
        roles: [],
      ])
    end

    it "returns only a subset of attributes for persisted records" do
      policy = described_class.new(admin, create(:game_access_invitation, game: game))

      expect(policy.permitted_attributes).to eq([
        :invited_emails,
        :open,
        :active,
      ])
    end
  end

  permissions :create? do
    let(:record) { invitation }

    permit :admin
    deny   :admin_inactive
    deny   :player
    deny   :roleless
    deny   :no_grant
  end

end
