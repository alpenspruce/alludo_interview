require 'rails_helper'

RSpec.describe "[desktop] Game invitation acceptation", type: :system do
  let!(:district)  { create(:district, domains: ["my-district.com"]) }
  let!(:school)    { create(:school,                                  district: district, name: "Primary School") }
  let!(:game)      { create(:game, :published, :invited_players_only, district: district, name: "DOOM") }
  let(:other_user) { create(:user, :google_oauth2, :player) }
  let(:third_user) { create(:user, :google_oauth2, :player) }

  let!(:invitation) do
    create(:game_access_invitation, :closed,
           game: game,
           invited_emails: "user1@example.com, user2@example.com")
  end

  scenario "As registered user, I can accept game invitation" do
    sign_in_user other_user

    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user2@example.com"

    expect(page.current_path).to eq("/games/#{game.id}-doom")
    expect(page).to have_content("Welcome to DOOM!")

    within("#top-players") do
      within("#top_player_1") { expect(page).to have_content("1. You 0") }
    end

    GameAccess::InvitationAcceptation.where(user: other_user).last.tap do |acceptation|
      expect(acceptation.invitation).to    eq(invitation)
      expect(acceptation.invited_email).to eq("user2@example.com")
    end

    expect(other_user.game_access_grants.size).to eq(1)
    other_user.game_access_grants.last.tap do |grant|
      expect(grant).to            be_active
      expect(grant.game).to       eq(game)
      expect(grant.invitation).to eq(invitation)
      expect(grant.roles).to      eq(["player"])
    end
  end

  scenario "As registered user, I see error message when there's something off with the invitation" do
    sign_in_user other_user

    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user999999@example.com"
    expect(page).to have_content("This is an invitation for closed group only. Unfortunately you're not included.")

    invitation.update!(active: false)
    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user2@example.com"
    expect(page).to have_content("Invitation was deactivated by admin and you can no longer use it.")

    invitation.update!(active: true)
    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user2@example.com"
    expect(page).to have_content("Welcome to DOOM!")

    # He uses the same link again but that's fine.
    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user2@example.com"
    expect(page.current_path).to eq("/games/#{game.id}-doom")
    expect(page).to have_content("Play DOOM")

    # Stolen invitation link:
    sign_in_user third_user
    visit "/game_access/invitations/#{invitation.token}/accept?invited_email=user2@example.com"
    expect(page).to have_content("Invitation was already accepted. Has someone shared a link with you?")
  end

end
