require 'rails_helper'

RSpec.describe "[desktop] Game invitations", type: :system do
  include_context "Star Wars characters competition"

  let!(:jusd_doom_invitation_open) do
    create(:game_access_invitation, :open, :admin, :inactive,
           game: jusd_doom,
           invited_emails: "user2@example.com; user1@example.com")
  end

  before do
    sign_in_user sith
    visit game_access_invitations_path(jusd_doom)
  end

  scenario "As an admin, I can create new game invitation", js: true, active_job: true do
    find("a[href='/game_access/#{jusd_doom.id}-doom/invitations/new']").click

    wait_for_ajax
    within(".modal-dialog") do
      within(".modal-header") { expect(page).to have_content("Invitation") }

      js_check "Open?"
      js_check "achievement reviewer"
      fill_in "E-mails", with: "invited1@example.com\ninvited2@example.com"

      expect {
        find_button("Save").click
        wait_for_ajax
      }.to send_emails(
        ["invited1@example.com", "Darth Sidious invited you to DOOM"],
        ["invited2@example.com", "Darth Sidious invited you to DOOM"],
      )
    end

    expect(page).to have_content("Invitation was successfully created")

    within("#game_access_invitations") do
      expect(page).to have_selector("tbody tr", count: 3)

      within("tbody tr:nth-child(3)") { expect(page).to have_content("Darth Sidious ACHIEVEMENT REVIEWER 2 invited, 0 accepted content_copy done done") }
    end
  end

end
