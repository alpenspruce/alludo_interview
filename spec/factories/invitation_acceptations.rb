FactoryBot.define do
  factory :game_access_invitation_acceptation, class: GameAccess::InvitationAcceptation do
    association :invitation, factory: :game_access_invitation
    user
    invited_email { user.email }
  end
end
