FactoryBot.define do
  factory :game_access_invitation, class: GameAccess::Invitation do
    game
    creator { game.creator }
    active true
    open false
    roles ["player"]

    invited_emails "user2@example.com; user3@example.com user1@example.com"

    trait :closed do
      open false
    end

    trait :open do
      open true
    end

    trait :inactive do
      active false
    end

    trait :player do
      roles ["player"]
    end

    trait :admin do
      roles ["admin"]
    end

    trait :roleless do
      roles [ ]
    end

  end
end
