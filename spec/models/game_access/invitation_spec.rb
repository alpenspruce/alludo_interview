require 'rails_helper'

RSpec.describe GameAccess::Invitation, type: :model do

  describe "Validation" do
    it { should validate_presence_of(:roles) }

    context "when invitation is open" do
      let(:subject) { GameAccess::Invitation.new(open: true) }

      it { should_not validate_presence_of(:invited_emails) }
    end

    context "when invitation is closed" do
      let(:subject) { GameAccess::Invitation.new(open: false) }

      it { should validate_presence_of(:invited_emails) }

      it "validates correctness of provided e-mail addresses" do
        subject.invited_emails = %Q{john@example.com,"\ncopypastaerror,lucy@example.com}

        expect(subject).not_to be_valid
        expect(subject.errors[:invited_emails]).to contain_exactly(
          '"\"" doesn\'t look like a valid address',
          '"copypastaerror" doesn\'t look like a valid address'
        )
      end
    end
  end

  describe "Callbacks" do
    describe "#before_create" do
      let(:invitation) { build(:game_access_invitation) }

      it "generates secure token" do
        expect {
          invitation.save!
        }.to change { invitation.token }.from(nil).to(match(/\w{24}/))
      end
    end

    describe "after_commit" do
      let(:invitation) { build(:game_access_invitation) }

      describe "on :create" do
        let(:delivery) { instance_double(GameAccess::InvitationDelivery) }

        it "delivers invitation" do
          expect(GameAccess::InvitationDelivery).to receive(:new).with(invitation).and_return(delivery)
          expect(delivery).to receive(:deliver).and_return(true)

          invitation.save!
        end
      end
    end
  end

  describe "#invited_emails=" do
    let(:invitation) { GameAccess::Invitation.new }

    it "allows users to provide e-mails list in a messy format and normalizes it" do
      invitation.invited_emails = <<-EMAILS.strip_heredoc
        john@example.com,adam@example.com kate@example.com ,
         molly@example.com, bart@example.com,

        jack@example.com
        john@example.com
        amy@example.com; tom@example.com
        JACK@example.COM

        kate@example.com

      EMAILS

      expect(invitation.invited_emails).to eq(<<-EMAILS.strip_heredoc.chomp)
        JACK@example.COM
        adam@example.com
        amy@example.com
        bart@example.com
        jack@example.com
        john@example.com
        kate@example.com
        molly@example.com
        tom@example.com
      EMAILS
    end
  end

  describe "#invited_emails_array" do
    let(:invitation) { GameAccess::Invitation.new }

    it "returns array of invited emails" do
      expect(invitation.invited_emails_array).to eq([])

      invitation.invited_emails = "\n john@example.com,adam@example.com kate@example.com ,"
      expect(invitation.invited_emails_array).to eq(["adam@example.com", "john@example.com", "kate@example.com"])
    end
  end

  describe "#closed?" do
    it "returns true when invitation is not open to everyone" do
      invitation = GameAccess::Invitation.new(open: false)
      expect(invitation).to be_closed
    end

    it "returns false when invitation is open to everyone" do
      invitation = GameAccess::Invitation.new(open: true)
      expect(invitation).not_to be_closed
    end
  end

end
