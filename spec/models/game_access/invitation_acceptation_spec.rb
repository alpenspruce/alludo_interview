require 'rails_helper'

RSpec.describe GameAccess::InvitationAcceptation, type: :model do

  describe "Scopes" do
    describe ".by_created_at" do
      it "returns records sorted by created_at" do
        sql = GameAccess::InvitationAcceptation.by_created_at.to_sql
        expect(sql).to include('ORDER BY game_access_invitation_acceptations.created_at ASC NULLS LAST')
      end
    end
  end

end
