require 'rails_helper'

RSpec.describe GameAccess::AcceptInvitation do

  let!(:game) { create(:game) }
  let!(:user) { create(:user) }

  let(:open_invitation) do
    create(:game_access_invitation, :open, :admin, game: game)
  end

  let(:closed_invitation) do
    create(:game_access_invitation, :closed, :admin,
           game: game,
           invited_emails: "another_user@example.com, invited_user@example.com")
  end

  let(:invited_email) { "invited_user@example.com" }
  let(:invitation)    { open_invitation }

  let(:acceptation) { GameAccess::AcceptInvitation.new(user: user, invitation: invitation, invited_email: invited_email) }

  describe "Validation" do
    context "when user is blank" do
      let(:user) { nil }

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:user]).to include("can't be blank")
      end
    end

    context "when invitation is blank" do
      let(:invitation) { nil }

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:invitation]).to include("can't be blank")
      end
    end

    context "when invitation is no longer active" do
      before do
        invitation.update!(active: false)
      end

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:invitation]).to include("was deactivated by admin and you can no longer use it.")
      end
    end

    context "when user already accepted this open invitation in the past" do
      let!(:past_acceptation) { create(:game_access_invitation_acceptation, invitation: invitation, user: user) }

      it "returns true and does nothing because it's OK to click the link multiple times" do
        expect(acceptation.accept).to be(true)
        expect(acceptation.errors).to be_blank
      end
    end

    context "when someone stole an open invitation link with e-mail specified" do
      let!(:past_acceptation1) { create(:game_access_invitation_acceptation, invitation: invitation, invited_email: invited_email) }

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:invitation]).to include("was already accepted. Has someone shared a link with you?")
      end
    end

    context "when someone already accepted a closed invitation with the same e-mail in the past (stolen link)" do
      let(:invitation)        { closed_invitation }
      let!(:past_acceptation) { create(:game_access_invitation_acceptation, invitation: invitation, invited_email: invited_email) }

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:invitation]).to include("was already accepted. Has someone shared a link with you?")
      end
    end

    context "when invitation is closed and user's e-mail is not on the list" do
      let(:invitation)    { closed_invitation }
      let(:invited_email) { "hacker@gmail.com" }

      it "returns false and adds an error" do
        expect(acceptation.accept).to be(false)
        expect(acceptation.errors[:base]).to include("This is an invitation for closed group only. Unfortunately you're not included.")
      end
    end
  end

  describe "#accept" do
    context "when operation is valid" do
      context "when user has already accepted this invitation in the past" do
        let!(:past_acceptation) do
          create(:game_access_invitation_acceptation,
                 invitation: invitation,
                 user: user)
        end

        it "returns true and doesn't create any new Grants or invitation acceptation records" do
          expect {
            expect {
              expect(acceptation.accept).to be(true)
            }.not_to change { GameAccess::Grant.count }
          }.not_to change { GameAccess::InvitationAcceptation.count }
        end
      end

      context "when user had no previous access to a game" do
        it "creates new Grant and invitation acceptation records" do
          expect {
            expect {
              expect(acceptation.accept).to be(true)
            }.to change { GameAccess::Grant.count }.by(+1)
          }.to change { GameAccess::InvitationAcceptation.count }.by(+1)

          GameAccess::Grant.last.tap do |grant|
            expect(grant).to            be_active
            expect(grant.game).to       eq(game)
            expect(grant.user).to       eq(user)
            expect(grant.invitation).to eq(invitation)
            expect(grant.roles).to      eq(["player", "admin"])
          end

          GameAccess::InvitationAcceptation.last.tap do |acceptation_record|
            expect(acceptation_record.invitation).to    eq(invitation)
            expect(acceptation_record.user).to          eq(user)
            expect(acceptation_record.invited_email).to eq("invited_user@example.com")
          end
        end
      end

      context "when user had previous access to a game with some roles" do
        let!(:existing_grant) { create(:game_access_grant, :inactive, :player, user: user, game: game) }

        it "updates existing Grant record, merges the roles and activates the grant creating also acceptation record" do
          existing_grant.reload
          expect(existing_grant).not_to        be_active
          expect(existing_grant.invitation).to be_nil
          expect(existing_grant.roles).to      eq(["player"])

          expect {
            expect {
              expect(acceptation.accept).to be(true)
            }.not_to change { GameAccess::Grant.count }
          }.to change { GameAccess::InvitationAcceptation.count }.by(+1)

          existing_grant.reload
          expect(existing_grant).to            be_active
          expect(existing_grant.game).to       eq(game)
          expect(existing_grant.user).to       eq(user)
          expect(existing_grant.invitation).to eq(invitation)
          expect(existing_grant.roles).to      eq(["player", "admin"])

          GameAccess::InvitationAcceptation.last.tap do |acceptation_record|
            expect(acceptation_record.invitation).to    eq(invitation)
            expect(acceptation_record.user).to          eq(user)
            expect(acceptation_record.invited_email).to eq("invited_user@example.com")
          end
        end
      end

    end

    context "when operation is not valid" do
      let(:user) { nil }

      it "doesn't create any grants or invitation acceptation records" do
        expect(GameAccess::Grant.count).to                 eq(1)
        expect(GameAccess::InvitationAcceptation.count).to eq(0)

        expect(acceptation.accept).to be(false)
        expect(GameAccess::Grant.count).to                 eq(1)
        expect(GameAccess::InvitationAcceptation.count).to eq(0)
      end
    end

    context "when unexpected error happens" do
      before do
        expect(acceptation).to receive(:after_operation).and_wrap_original do |m, *args|
          m.call(*args)
          raise RuntimeError, "Oops"
        end
      end

      it "doesn't create new grant or invitation acceptation records" do
        expect(GameAccess::Grant.count).to                 eq(1)
        expect(GameAccess::InvitationAcceptation.count).to eq(0)

        expect { acceptation.accept }.to raise_error(RuntimeError)
        expect(GameAccess::Grant.count).to                 eq(1)
        expect(GameAccess::InvitationAcceptation.count).to eq(0)
      end
    end
  end

end
