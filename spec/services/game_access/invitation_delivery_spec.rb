require 'rails_helper'

RSpec.describe GameAccess::InvitationDelivery do

  let(:open_invitation) do
    create(:game_access_invitation, :open, :admin, invited_emails: "")
  end

  let(:closed_invitation) do
    create(:game_access_invitation, :closed,
           invited_emails: "second_user@example.com, first_user@example.com, third_user@example.com")
  end

  let(:invitation) { open_invitation }

  let(:delivery) { GameAccess::InvitationDelivery.new(invitation) }

  describe "Validation" do
    context "when invitation is blank" do
      let(:invitation) { nil }

      it "returns false and adds an error" do
        expect(delivery.deliver).to be_falsy
        expect(delivery.errors[:invitation]).to include("can't be blank")
      end
    end
  end

  describe "#emails_to_deliver_invitation_to" do
    context "when no e-mails were specified" do
      let(:invitation) { open_invitation }

      it "returns an empty list of e-mails" do
        create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "second_user@example.com")

        expect(delivery.emails_to_deliver_invitation_to).to eq([])
      end
    end

    context "when some e-mails were specified" do
      let(:invitation) { closed_invitation }

      it "returns a list of e-mails where invitation will be delivered" do
        create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "second_user@example.com")

        expect(delivery.emails_to_deliver_invitation_to).to match_array([
          "first_user@example.com",
          "third_user@example.com"
        ])
      end
    end
  end

  describe "#deliver" do
    context "when operation is valid" do
      context "when invited e-mails list was specified" do
        let(:invitation) { closed_invitation }

        it "enqueues invitation e-mail delivery for people that haven't accept it yet" do
          create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "first_user@example.com")

          expect(GameAccess::InvitationMailer).to receive(:invite_user)
            .with(invitation, "second_user@example.com")
            .and_return(double(deliver_later: true))
          expect(GameAccess::InvitationMailer).to receive(:invite_user)
            .with(invitation, "third_user@example.com")
            .and_return(double(deliver_later: true))

          expect(delivery.deliver).to be(true)
        end

        it "doesn't enqueue invitation e-mail delivery if all people from the list accepted the invitation already" do
          create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "first_user@example.com")
          create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "second_user@example.com")
          create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "third_user@example.com")

          expect(GameAccess::InvitationMailer).not_to receive(:invite_user)

          expect(delivery.deliver).to be(true)
        end
      end

      context "when no invited emails are specified" do
        let(:invitation) { open_invitation }

        it "doesn't enqueue any e-mail deliveries" do
          create(:game_access_invitation_acceptation, invitation: invitation, invited_email: "first_user@example.com")

          expect(GameAccess::InvitationMailer).not_to receive(:invite_user)

          expect(delivery.deliver).to be(true)
        end
      end
    end

    context "when operation is not valid" do
      let(:invitation) { nil }

      it "doesn't enqueue any e-mails delivery" do
        expect(GameAccess::InvitationMailer).not_to receive(:invite_user)
        expect(delivery.deliver).to be(false)
      end
    end
  end

end
