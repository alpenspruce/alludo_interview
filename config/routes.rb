# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :game_access do
    scope ":game_id" do
      resources :invitations, except: [:destroy]
    end

    resources :invitations, only: [] do
      member do
        get :accept
      end
    end
  end
end
