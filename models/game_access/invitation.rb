# frozen_string_literal: true

# == Schema Information
#
# Table name: game_access_invitations
#
#  id             :bigint           not null, primary key
#  game_id        :bigint           not null
#  token          :string           not null
#  roles          :string           default([]), not null, is an Array
#  creator_id     :bigint           not null
#  invited_emails :text
#  open           :boolean          default(FALSE)
#  active         :boolean          default(TRUE)
#
module GameAccess
  class Invitation < ::ApplicationRecord

    ROLES = %w[player admin].freeze


    has_secure_token

    # Associations
    belongs_to :game
    belongs_to :creator,    class_name: "::User"
    has_many :acceptations, class_name: "GameAccess::InvitationAcceptation", dependent: :destroy

    validates :roles,          presence: true
    validates :invited_emails, presence: { if: :closed? }
    validate :all_invited_emails_are_valid, if: :closed?

    after_save :deliver_invitation, on: :create

    validate :allow_only_recognized_roles

    def invited_emails=(new_emails)
      normalized_emails = new_emails.to_s.split(/[,;]\s?|\s/).reject(&:blank?).sort.uniq
      super(normalized_emails.join("\n"))
    end

    def invited_emails_array
      invited_emails.to_s.split
    end

    def closed?
      !open
    end

    def roles=(new_roles)
      super Array(new_roles).reject(&:blank?)
    end

    def player?
      roles.include?("player")
    end

    def admin?
      roles.include?("admin")
    end

    def roleless?
      roles.empty?
    end

    private

    def allow_only_recognized_roles
      errors.add(:roles, :inclusion) if Array(roles) - ROLES.present?
    end

    private

    def all_invited_emails_are_valid
      invited_emails_array.each do |invited_email|
        unless invited_email.match?(URI::MailTo::EMAIL_REGEXP)
          errors.add(:invited_emails, "#{invited_email.inspect} doesn't look like a valid address")
        end
      end
    end

    def deliver_invitation
        InvitationDelivery.new(self).deliver
    end


  end
end
