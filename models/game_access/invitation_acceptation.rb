# frozen_string_literal: true

# == Schema Information
#
# Table name: game_access_invitation_acceptations
#
#  id            :bigint           not null, primary key
#  invitation_id :bigint           not null
#  user_id       :bigint           not null
#  invited_email :string
#  created_at    :datetime
#
module GameAccess
  class InvitationAcceptation < ::ApplicationRecord

    # Associations
    belongs_to :invitation, class_name: "GameAccess::Invitation"
    belongs_to :user

    scope :by_created_at, ->(direction = :asc) { order("created_at #{direction} NULLS LAST") }

  end
end
