module GameAccess
  class InvitationMailer < ::ApplicationMailer

    def invite_user(invitation, invited_email)
      @invitation = invitation
      @game       = invitation.game
      @invitator  = invitation.creator

      @acceptation_url = accept_game_access_invitation_url(invitation.token, invited_email: invited_email)

      mail to: invited_email, subject: "#{@invitator.full_name} invited you to #{@game.name}"
    end

  end
end
