class CrateGameAccessInvitations < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

    create_table :game_access_invitations do |t|
      t.references :game,    null: false, index: true
      t.uuid :token,         null: false, default: "gen_random_uuid()"
      t.string :roles,       null: false, default: [], array: true
      t.references :creator, null: false, index: true
      t.text :invited_emails
      t.boolean :open,       default: false
      t.boolean :active,     default: true
    end
  end
end
