class CreateGameAccessInvitationAcceptations < ActiveRecord::Migration[5.2]
  def change
    create_table :game_access_invitation_acceptations do |t|
      t.references :invitation, null: false, index: false
      t.references :user,       null: false,
      t.string :invited_email
    end

    add_index :game_access_invitation_acceptations, [:invitation_id, :invited_email], unique: true,
      name: :index_game_access_invitation_acceptations_on_id_and_email
    add_index :game_access_invitation_acceptations, [:invitation_id, :user_id], unique: true,
      name: :index_game_access_invitation_acceptations_on_id_and_user_id
  end
end
