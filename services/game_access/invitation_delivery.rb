module GameAccess
  class InvitationDelivery < ::Service

    validates :invitation, presence: true

    def initialize(invitation)
      @invitation = invitation
    end

    def emails_to_deliver_invitation_to
      invited_emails - already_accepted_invitation_emails
    end

    def deliver
      return false unless valid?
      return true if invited_emails.blank?

      ActiveRecord::Base.transaction do
        emails_to_deliver_invitation_to.each do |invited_email|
          GameAccess::InvitationMailer.invite_user(invitation, invited_email).deliver_later
        end

        after_operation
      end

      true
    end

    private

    attr_reader :invitation

    def after_operation
    end

    def invited_emails
      invitation.invited_emails_array
    end

    def already_accepted_invitation_emails
      invitation.acceptations.pluck(:invited_email)
    end

  end
end
