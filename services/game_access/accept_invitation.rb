module GameAccess
  class AcceptInvitation < ::Service

    validates :user,       presence: true
    validates :invitation, presence: true

    validate :check_invitation_is_active
    validate :check_email_presence_for_closed_invitations
    validate :check_invitation_link_wasnt_stolen

    def initialize(user:, invitation:, invited_email:)
      @user          = user
      @invitation    = invitation
      @invited_email = invited_email
    end

    def accept
      return true if already_accepted_by_user?

      ActiveRecord::Base.transaction do
        return false unless valid?

        create_invitation_acceptation
        create_or_update_access_grant

        after_operation
      end

      true
    end

    private

    attr_reader :user, :invitation, :invited_email

    def after_operation
    end

    def already_accepted_by_user?
      return false if invitation.blank?
      return false if user.blank?

      GameAccess::InvitationAcceptation.where(invitation: invitation, user: user).exists?
    end

    def check_invitation_is_active
      return if errors.include?(:invitation)

      unless invitation.active?
        errors.add(:invitation, :invitation_no_longer_active)
      end
    end

    def check_email_presence_for_closed_invitations
      return if errors.include?(:invitation)
      return if invitation.open?

      if invited_email.blank? || invitation.invited_emails.exclude?(invited_email)
        errors.add(:base, :not_included_on_invited_emails_list)
      end
    end

    def check_invitation_link_wasnt_stolen
      return if errors.present?

      accepted_by_email = GameAccess::InvitationAcceptation.where(invitation: invitation, invited_email: invited_email).exists?

      if invited_email.present? && accepted_by_email
        errors.add(:invitation, :already_accepted)
      end
    end

    def create_invitation_acceptation
      GameAccess::InvitationAcceptation.create!(
        user: user,
        invitation: invitation,
        invited_email: invited_email
      )
    end

    def game
      invitation.game
    end

    def create_or_update_access_grant
      grant = game.access_grants.find_or_initialize_by(user: user)

      # When user already has a grant then merge roles with new invitation roles.
      combined_roles = (grant.roles + invitation.roles).uniq
      grant.update!(active: true, roles: combined_roles, invitation: invitation)
    end

  end
end
