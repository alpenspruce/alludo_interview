module GameAccess
  class InvitationsController < ::ApplicationController
    before_action :load_resources, except: [:accept]

    def index
      authorize @game, :list_invitations?
      @invitations = @game.invitations
    end

    def new
      @invitation = @game.invitations.new
      authorize @invitation

      respond_to do |format|
        format.js
      end
    end

    def create
      @invitation = @game.invitations.new
      @invitation.assign_attributes(permitted_attributes(@invitation))
      @invitation.creator = current_user
      authorize @invitation

      respond_to do |format|
        format.js {
          if @invitation.save
            @redirect_url   = game_access_invitations_path(@game)
            flash[:success] = t('.success')
          end
        }
      end
    end

    def accept
      invitation = GameAccess::Invitation.find_by!(token: params[:id])

      unless signed_in?
        redirect_to sign_in_path
        return
      end

      @acceptation = GameAccess::AcceptInvitation.new(
        user: current_user,
        invitation: invitation,
        invited_email: params[:invited_email]
      )

      if @acceptation.accept
        redirect_to game_path(invitation.game)
      else
        # Display template with errors
      end
    end

    private

    def load_resources
      @game       = current_user.available_games.find(params[:game_id])
      @invitation = @game.invitations.find(params[:id]) if params[:id].present?
    end

  end
end
