module GameAccess
  class InvitationPolicy < ::ApplicationPolicy

    def permitted_attributes
      if record.new_record?
        [
          :invited_emails,
          :open,
          :active,
          roles: [],
        ]
      else
        [
          :invited_emails,
          :open,
          :active,
        ]
      end
    end

    def create?
      grant&.admin?
    end

    private

    def game
      record.game
    end

    def grant
      @@grant ||= GameAccess::Grant.active_for(game, user)
    end

  end
end
